from django.db import models


class Config(models.Model):
    items_per_line = models.IntegerField(default=3)


class Component(models.Model):

    STATUS_CHOICES = [
        (1, 'Desconhecido'),
        (2, 'Operacional'),
        (3, 'Indisponibilidade Parcial'),
        (4, 'Indisponibilidade Total'),
    ]

    name = models.CharField(max_length=255)
    hostname = models.CharField(max_length=255, null=True, blank=True)
    display_name = models.CharField(max_length=255, blank=True)
    description = models.TextField(max_length=255, null=True, blank=True)
    tags = models.CharField(max_length=255, null=True, blank=True)
    url = models.CharField(max_length=255, null=True, blank=True)
    enabled = models.BooleanField(default=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=2)
